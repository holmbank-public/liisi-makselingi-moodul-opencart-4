<?php

namespace Opencart\System\Library;

use CurlHandle;

/**
 * Class handles API request to the Holmbank backend service.
 */
class Holmbank {

    /** @var string Url designed for processing API request in development mode. */
    private string $development_api_url = 'https://wiremocks.nonprod.holmbank.ee/api/partners/public/';

    /** @var string Url designed for processing API request in live mode. */
    private string $live_api_url = 'https://gateway-partner.hbc.holmbank.ee/api/partners/public/';

    /** @var string Url for processing API request. */
    private string $api_url;

    /** @var string unique public key for API access. */
    private string $x_payment_link_key;

    /**
     * Holmbank API constructor.
     */
    public function __construct($holmbank_config)
    {
        $this->x_payment_link_key = $holmbank_config["payment_holmbank_x_payment_link_key"] ?? '';
        if (isset($holmbank_config["payment_holmbank_live_activity_status"])) {
            $this->api_url = $holmbank_config["payment_holmbank_live_activity_status"] ? $this->live_api_url : $this->development_api_url;
        } else {
            $this->api_url = $this->development_api_url;
        }
    }

    /**
     * Method prepares request to the API. Insert headers etc.
     * @param string $action Type of action to be performed with request.
     * @param string|null $x_payment_link_req_id Unique transaction key.
     */
    public function prepareRequest(string $action, string $x_payment_link_req_id = null): CurlHandle|bool
    {
        $fullRequestUrl = $this->api_url . $action;
        $requestHeaders = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'x-payment-link-key: ' . $this->x_payment_link_key,
        );

        // If request unique ID required, then put it into array
        if ($x_payment_link_req_id !== null)
        {
            $requestHeaders[] = "x-payment-link-req-id: " . $x_payment_link_req_id;
        }

        $curl = curl_init($fullRequestUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $requestHeaders);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        return $curl;
    }

    /**
     * Method makes request to API and fetches all Holmbank Product Loans.
     * @return mixed JSON decoded response.
     */
    public function getLoanProducts(): mixed
    {
        $curl = $this->prepareRequest("payment-link/products");
        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);
    }

    /**
     * Method makes request to API with data needed to start loan process.
     * @param $x_payment_link_req_id string Unique transaction key.
     * @param $fields array Defines fields (data) to put in body.
     */
    public function postLoanStart(string $x_payment_link_req_id, array $fields = []): bool|string
    {
        $curl = $this->prepareRequest("payment-link/orders", $x_payment_link_req_id);
        $dataFields = json_encode($fields);

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataFields);

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }
}