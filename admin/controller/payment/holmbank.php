<?php

namespace Opencart\Admin\Controller\Extension\Holmbank\Payment;


/**
 * Holmbank Checkout Extension BO Controller.
 */
class Holmbank extends \Opencart\System\Engine\Controller
{
    private $error = [];

    /**
     * Executed on Extension install - executes SQL queries.
     * @return void
     */
    public function install() {
        if ($this->user->hasPermission('modify', 'extension/payment')) {
            // Register hook to add calculator on product page.
            $this->load->model('setting/event');
            $this->model_setting_event->addEvent(array(
                'code'       => 'holmbank_modify_product_page',
                'description' => 'Add holmbank calculator to product page',
                'trigger'    => 'catalog/view/product/product/before',
                'action'     => 'extension/holmbank/payment/holmbank.modifyProductPage',
                'status'     => 1,
                'sort_order' => 1
            ));

            $this->model_setting_event->addEvent(array(
                'code'       => 'holmbank_modify_product_page_header',
                'description' => 'Add holmbank calculator css to product page',
                'trigger'    => 'catalog/controller/product/product/before',
                'action'     => 'extension/holmbank/payment/holmbank.addProductPageHeader',
                'status'     => 1,
                'sort_order' => 1
            ));

            // Create Tables and Insert initial Data.
            $this->load->model('extension/holmbank/payment/holmbank');
            $this->model_extension_holmbank_payment_holmbank->install();

            // Create Payment Related Order Statuses.
            $this->cache->delete('order_status');
            $this->cache->delete('order_status.language');

            $this->load->model('localisation/order_status');
            $this->load->model('setting/setting');

            $status_name = 'Holmbank payment awaiting';

            $order_status_data = array(
                'order_status' => array(
                    $this->config->get('config_language_id') => array(
                        'name' => $status_name,
                        'comment' => '',
                        'sort_order' => 0
                    )
                )
            );

            $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

            foreach ($order_statuses as $order_status) {
                if ($order_status['name'] == $status_name) {
                    return;
                }
            }

            $this->model_localisation_order_status->addOrderStatus($order_status_data);
        }
    }

    /**
     * Executed on Extension uninstall - executes SQL queries.
     * @return void
     */
    public function uninstall() {
        if ($this->user->hasPermission('modify', 'extension/payment')) {
            // Remove hook for displaying calculator on product page.
            $this->load->model('setting/event');
            $this->model_setting_event->deleteEventByCode('holmbank_modify_product_page');
            $this->model_setting_event->deleteEventByCode('holmbank_modify_product_page_header');

            // Remove Module Related Tables.
            $this->load->model('extension/holmbank/payment/holmbank');
            $this->model_extension_holmbank_payment_holmbank->uninstall();
        }
    }

    /**
     * Back Office View.
     * @return void
     */
    public function index(): void {
        $this->load->model('setting/setting');
        $this->load->model('extension/holmbank/payment/holmbank');
        $this->load->language('extension/holmbank/payment/holmbank');
        $this->document->setTitle($this->language->get('heading_title_main'));

        // Load Extension Config
        $this->load->model('setting/setting');
        $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

        $data['config_setting'] = $mySettingValue;

        // Actions
        $data['href_back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
        $data['href_save'] = $this->url->link('extension/holmbank/payment/holmbank|save', 'user_token=' . $this->session->data['user_token']);
        $data['href_add'] = $this->url->link('extension/holmbank/payment/holmbank|add', 'user_token=' . $this->session->data['user_token']);
        $this->config->get('payment_holmbank_x_payment_link_key');

        $data['payment_my_response'] = $this->config->get('payment_my_response');
        $data['payment_my_geo_zone_id'] = $this->config->get('payment_my_geo_zone_id');
        $data['is_key_initialized'] = $mySettingValue['payment_holmbank_x_payment_link_key'] ?? false;

        // Setting Page Header Data
        $data['breadcrumbs'] = [
            array('text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
            array('text' => $this->language->get('text_extensions'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
            array('text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/holmbank/payment/holmbank', 'user_token=' . $this->session->data['user_token']))
        ];
        $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

        // Setting Merchant Table Data
        $data['merchant_product_table_headers'] = [
            $this->language->get('table_id_column_title'),
            $this->language->get('table_id_column_type'),
            $this->language->get('table_id_column_name'),
            $this->language->get('table_id_column_description'),
            $this->language->get('table_id_column_active'),
            $this->language->get('table_id_column_is_calculator_displayed'),
            $this->language->get('table_id_column_interest_rate'),
            $this->language->get('table_id_column_payment_period'),
            $this->language->get('table_id_column_calculator_text'),
            $this->language->get('table_id_column_calculator_link'),
            ''
        ];

        // Get Al Merchant Products Located in DB.
        $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
        $merchantProducts = $holmbankRepository->getAllMerchantProducts();

        // Remove image path URL from display.
        foreach ($merchantProducts as $key => $merchantProduct) {
            unset($merchantProducts[$key]['logo']);

            // Change to icons.
            $merchantProducts[$key]['is_enabled'] = $merchantProducts[$key]['is_enabled'] == 1 ? '&#10004' : '&#10060';
            $merchantProducts[$key]['is_calculator_displayed'] = $merchantProducts[$key]['is_calculator_displayed'] == 1 ? '&#10004' : '&#10060';

            $merchantProducts[$key]['editUrl'] = $this->url->link('extension/holmbank/payment/holmbank|edit'
                , 'user_token=' . $this->session->data['user_token'] . '&id=' . $merchantProducts[$key]['id_merchant_product']);
            $merchantProducts[$key]['deleteUrl'] = $this->url->link('extension/holmbank/payment/holmbank|delete'
                , 'user_token=' . $this->session->data['user_token'] . '&id=' . $merchantProducts[$key]['id_merchant_product']);
        }

        $data['merchant_product_table_data'] = $merchantProducts;

        // Add Common Components to Extension Settings View.
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->document->addScript('extension/holmbank/admin/view/javascript/holmbank.js');

        $this->response->setOutput($this->load->view('extension/holmbank/payment/holmbank', $data));
    }

    /**
     * Method handles basic module settings save.
     * @return void
     */
    public function save(): void {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('payment_holmbank', $this->request->post);
            $this->response->redirect($this->url->link('extension/holmbank/payment/holmbank', 'user_token=' . $this->session->data['user_token']));
        }
    }

    public function edit(): void {
        $this->load->model('extension/holmbank/payment/holmbank');
        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
            $merchant_product = $holmbankRepository->getMerchantProductById($this->request->get['id']);
            $this->load->model('setting/setting');
            $this->load->language('extension/holmbank/payment/holmbank');
            $this->document->setTitle($this->language->get('heading_title_main'));

            // API Controller
            $this->load->model('setting/setting');
            $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

            require_once DIR_EXTENSION . 'holmbank/system/library/holmbank.php';
            $holmbank_api = new \Opencart\System\Library\Holmbank($mySettingValue);
            $merchant_product_types = $holmbank_api->getLoanProducts();
            $data['merchant_product_data'] = $merchant_product;
            $data['merchant_product_id'] = $this->request->get['id'];
            $data['merchant_product_types'] = $merchant_product_types;
            $data['form_type_label'] = $this->language->get('form_type_label');
            $data['form_name_label'] = $this->language->get('form_name_label');
            $data['form_description_label'] = $this->language->get('form_description_label');
            $data['form_active_label'] = $this->language->get('form_active_label');
            $data['form_is_calculator_displayed_label'] = $this->language->get('form_is_calculator_displayed_label');
            $data['form_interest_rate_label'] = $this->language->get('form_interest_rate_label');
            $data['form_payment_period_label'] = $this->language->get('form_payment_period_label');
            $data['form_calculator_text_label'] = $this->language->get('form_calculator_text_label');
            $data['form_calculator_link_label'] = $this->language->get('form_calculator_link_label');

            // Actions
            $data['href_edit_merchant_product'] = $this->url->link('extension/holmbank/payment/holmbank|edit', 'user_token=' . $this->session->data['user_token']);

            // Setting Page Header Data
            $data['back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
            $data['breadcrumbs'] = [
                array('text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('text_extensions'),
                    'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
                array('text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('extension/holmbank/payment/holmbank', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('merchant_product_add_title'),
                    'href' => $this->url->link('extension/holmbank/payment/holmbank|add', 'user_token=' . $this->session->data['user_token']))
            ];
            $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

            // Add Common Components to Extension Settings View.
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->document->addScript('extension/holmbank/admin/view/javascript/holmbank.js');

            $this->response->setOutput($this->load->view('extension/holmbank/payment/merchantproduct', $data));
        } elseif($this->request->server['REQUEST_METHOD'] == 'POST') {
            // Save  Merchant Product to DB.
            $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
            $holmbankRepository->updateMerchantProduct($this->request->post);

            $this->index();
        }
    }

    /**
     * Method handles GET and POST request to/from merchant product addition page.
     * @return void
     */
    public function add(): void {
        $this->load->model('extension/holmbank/payment/holmbank');
        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            $this->load->model('setting/setting');
            $this->load->language('extension/holmbank/payment/holmbank');
            $this->document->setTitle($this->language->get('heading_title_main'));

            // API Controller
            require_once DIR_EXTENSION . 'holmbank/system/library/holmbank.php';
            $this->load->model('setting/setting');
            $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

            $holmbank_api = new \Opencart\System\Library\Holmbank($mySettingValue);
            $merchant_product_types = $holmbank_api->getLoanProducts();
            $data['merchant_product_types'] = $merchant_product_types;
            $data['form_type_label'] = $this->language->get('form_type_label');
            $data['form_name_label'] = $this->language->get('form_name_label');
            $data['form_description_label'] = $this->language->get('form_description_label');
            $data['form_active_label'] = $this->language->get('form_active_label');
            $data['form_is_calculator_displayed_label'] = $this->language->get('form_is_calculator_displayed_label');
            $data['form_interest_rate_label'] = $this->language->get('form_interest_rate_label');
            $data['form_payment_period_label'] = $this->language->get('form_payment_period_label');
            $data['form_calculator_text_label'] = $this->language->get('form_calculator_text_label');
            $data['form_calculator_link_label'] = $this->language->get('form_calculator_link_label');

            // Actions
            $data['href_add_merchant_product'] = $this->url->link('extension/holmbank/payment/holmbank|add', 'user_token=' . $this->session->data['user_token']);

            // Setting Page Header Data
            $data['back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
            $data['breadcrumbs'] = [
                array('text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('text_extensions'),
                    'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
                array('text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('extension/holmbank/payment/holmbank', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('merchant_product_add_title'),
                    'href' => $this->url->link('extension/holmbank/payment/holmbank|add', 'user_token=' . $this->session->data['user_token']))
            ];
            $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

            // Add Common Components to Extension Settings View.
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->document->addScript('extension/holmbank/admin/view/javascript/holmbank.js');

            $this->response->setOutput($this->load->view('extension/holmbank/payment/merchantproduct', $data));
        } elseif($this->request->server['REQUEST_METHOD'] == 'POST') {
            // Save  Merchant Product to DB.
            $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
            $holmbankRepository->addMerchantProduct($this->request->post);

            $this->index();
        }
    }

    /**
     * Method handles GET request to/from merchant product addition page.
     * @return void
     */
    public function delete(): void {
        // Delete Merchant Product to DB.
        $this->load->model('extension/holmbank/payment/holmbank');
        $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
        $holmbankRepository->deleteMerchantProduct($this->request->get['id']);

        $this->index();
    }
}