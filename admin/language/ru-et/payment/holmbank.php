<?php
// Heading
$_['heading_title']                         = 'Интеграция с Holmbank Checkout (настоятельно рекомендуется))';
$_['heading_title_main']                    = 'Интеграция с Holmbank Checkout';

$_['text_extensions']                       = 'Расширения';
$_['text_edit']                             = 'Изменить Holmbank';
$_['x_payment_link_key_text']               = 'API ключ';
$_['is_live_status_text']                   = 'Live режим';
$_['is_module_active']                      = 'Статус модуля';
$_['api_key_input_placeholder']             = 'Пожалуйста, вставьте здесь свой ключ API..';

$_['table_id_column_title']                 = 'ID';
$_['table_id_column_type']                  = 'Тип';
$_['table_id_column_name']                  = 'Название';
$_['table_id_column_description']           = 'Описание';
$_['table_id_column_active']                = 'Активность';
$_['table_id_column_is_calculator_displayed'] = 'Изображение калькулятора';
$_['table_id_column_interest_rate']         = 'Процентная ставка';
$_['table_id_column_payment_period']        = 'Период оплаты';
$_['table_id_column_calculator_text']       = 'Текст калькулятора';
$_['table_id_column_calculator_link']       = 'Линк калькулятора';

$_['form_type_label']                        = 'Тип';
$_['form_name_label']                        = 'Название';
$_['form_description_label']                 = 'Описание';
$_['form_logo_label']                        = 'URL картинки';
$_['form_active_label']                      = 'Активность';
$_['form_is_calculator_displayed_label']     = 'Изображение калькулятора';
$_['form_interest_rate_label']               = 'Процентная ставка';
$_['form_payment_period_label']              = 'Период оплаты';
$_['form_calculator_text_label']             = 'Текст калькулятора';
$_['form_calculator_link_label']             = 'Линк калькулятора';
$_['merchant_product_add_title']             = 'Создать новый продукт продавца';

$_['create_merchant_product_message']        = 'Нажмите, чтобы добавить новый продукт продавца';