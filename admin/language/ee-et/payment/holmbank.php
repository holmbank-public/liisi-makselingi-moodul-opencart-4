<?php
// Heading
$_['heading_title']                         = 'Holmbank Checkout integreerimine (väga soovitatav)';
$_['heading_title_main']                    = 'Holmbank Checkout integreerimine';

$_['text_extensions']                       = 'Laiendused';
$_['text_edit']                             = 'Muuda Holmbank';
$_['x_payment_link_key_text']               = 'API võti';
$_['is_live_status_text']                   = 'Aktiivsuse staatus';
$_['is_module_active']                      = 'Mooduli status';
$_['api_key_input_placeholder']             = 'Palun sisestage siia oma API võti..';

$_['table_id_column_title']                 = 'ID';
$_['table_id_column_type']                  = 'Tüüp';
$_['table_id_column_name']                  = 'Nimi';
$_['table_id_column_description']           = 'Kirjeldus';
$_['table_id_column_active']                = 'Aktiivne';
$_['table_id_column_is_calculator_displayed'] = 'Kalkulaatori staatus';
$_['table_id_column_interest_rate']         = 'Intress';
$_['table_id_column_payment_period']        = 'Makseperiood';
$_['table_id_column_calculator_text']       = 'Kalkulaatori tekst';
$_['table_id_column_calculator_link']       = 'Kalkulaatori link';

$_['form_type_label']                        = 'Tüüp';
$_['form_name_label']                        = 'Nimi';
$_['form_description_label']                 = 'Kirjeldus';
$_['form_logo_label']                        = 'Logo URL';
$_['form_active_label']                      = 'Aktiivsuse staatus';
$_['form_is_calculator_displayed_label']     = 'Kalkulaatori staatus';
$_['form_interest_rate_label']               = 'Intress';
$_['form_payment_period_label']              = 'Makseperiood';
$_['form_calculator_text_label']             = 'Kalkulaatori tekst';
$_['form_calculator_link_label']             = 'Kalkulaatori link';
$_['merchant_product_add_title']             = 'Looge uus kaupmehe toode';

$_['create_merchant_product_message']        = 'Klõpsake uue kaupmehe toote lisamiseks';