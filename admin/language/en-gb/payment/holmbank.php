<?php
// Heading
$_['heading_title']                         = 'Holmbank Checkout Integration (Highly Recommended)';
$_['heading_title_main']                    = 'Holmbank Checkout Integration';

$_['text_extensions']                       = 'Extensions';
$_['text_edit']                             = 'Edit Holmbank';
$_['x_payment_link_key_text']               = 'API key';
$_['is_live_status_text']                   = 'Live mode';
$_['is_module_active']                      = 'Module status';
$_['api_key_input_placeholder']             = 'Please insert your API key here..';

$_['table_id_column_title']                 = 'ID';
$_['table_id_column_type']                  = 'Type';
$_['table_id_column_name']                  = 'Name';
$_['table_id_column_description']           = 'Description';
$_['table_id_column_active']                = 'Active';
$_['table_id_column_is_calculator_displayed'] = 'Calculator display';
$_['table_id_column_interest_rate']         = 'Interest rate';
$_['table_id_column_payment_period']        = 'Payment period';
$_['table_id_column_calculator_text']       = 'Calculator text';
$_['table_id_column_calculator_link']       = 'Calculator link';

$_['form_type_label']                        = 'Type';
$_['form_name_label']                        = 'Name';
$_['form_description_label']                 = 'Description';
$_['form_logo_label']                        = 'Logo URL';
$_['form_active_label']                      = 'Active';
$_['form_is_calculator_displayed_label']    = 'Calculator display';
$_['form_interest_rate_label']               = 'Interest rate';
$_['form_payment_period_label']              = 'Payment period';
$_['form_calculator_text_label']             = 'Calculator text';
$_['form_calculator_link_label']             = 'Calculator link';
$_['merchant_product_add_title']             = 'Create new merchant product';

$_['create_merchant_product_message']        = 'Click to add new merchant product';