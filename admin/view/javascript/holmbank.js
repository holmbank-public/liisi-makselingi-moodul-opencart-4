
$(document).ready(function() {
    $('#settings_submit').on('click', function() {
        $('#form-payment').submit();
    });
});

// Insert loaded values from API.
document.addEventListener("DOMContentLoaded", function(event) {
    let paymentOption = $('#input-merchant-product-type').val();

    $('#input-merchant-product-type').on('change', function(e) {
        let paymentOption = $('#input-merchant-product-type').val();

        for (const [key, value] of Object.entries(JSON.parse('{{ merchant_product_types | json_encode | raw }}'))) {
            if(value.type == paymentOption) {
                $('#input-form-name-label').val(value.name);
                $('#input-form-logo-label').val(value.logoUrl);
            }
        }
    })
});