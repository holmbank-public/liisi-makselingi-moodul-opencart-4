<?php

namespace Opencart\Admin\Model\Extension\Holmbank\Payment;


/**
 * Repository to process Database operations with Merchant Product Table.
 */
class Holmbank extends \Opencart\System\Engine\Model {

    /**
     * Extension install.
     * @return void
     */
    public function install() {
        $sql_queries = array();

        $sql_queries[] = 'CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'holmbank_merchant_product` (
            `id_merchant_product` INT(11) NOT NULL AUTO_INCREMENT,
            `type` VARCHAR(256) NOT NULL,
            `name` VARCHAR(256),
            `description` VARCHAR(512),
            `logo` VARCHAR(256),
            `is_enabled` TINYINT(1) NOT NULL DEFAULT 0,
            `is_calculator_displayed` TINYINT(1) NOT NULL DEFAULT 0,
            `interest_rate` FLOAT,
            `payment_period` FLOAT,
            `calculator_text` VARCHAR(256),
            `calculator_link` VARCHAR(256),
            PRIMARY KEY  (`id_merchant_product`)
        )';

        $sql_queries[] = 'CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'holmbank_orders` (
            `id_shop` INT(11) NOT NULL,
            `id_merchant_order` INT(11) NOT NULL,
            `id_holmbank_order` VARCHAR(256) NOT NULL,
            `x_payment_link_req_id` VARCHAR(256) NOT NULL,
            PRIMARY KEY  (`id_merchant_order`)
        )';

        foreach ($sql_queries as $sql_query) {
            $this->db->query($sql_query);
        }
    }

    /**
     * Extension unistall.
     * @return array
     */
    public function uninstall() {
        $sql_queries = array();

        $sql_queries[] = 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'holmbank_merchant_product';
        $sql_queries[] = 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'holmbank_orders';

        foreach ($sql_queries as $sql_query) {
            $this->db->query($sql_query);
        }
    }

    /**
     * Method process insertion of merchant product in Database.
     * @param $merchantProduct array Merchant product data to be inserted in Database.
     * @return mixed boolean as indicator of successful operation.
     */
    public function addMerchantProduct(array $merchantProduct): mixed
    {
        $query = $this->db->query(sprintf("INSERT INTO %sholmbank_merchant_product 
            (type, name, description, logo, is_enabled, is_calculator_displayed, interest_rate, payment_period, calculator_text, calculator_link) VALUES 
            ('%s','%s','%s','%s','%s','%s','%s','%s', '%s', '%s')", DB_PREFIX, $merchantProduct['type'], $merchantProduct['name'],
            $merchantProduct['description'], $merchantProduct['logo'], (bool)$merchantProduct['is_enabled'], (bool)$merchantProduct['is_calculator_displayed'],
            (float)$merchantProduct['interest_rate'], (float)$merchantProduct['payment_period'], $merchantProduct['calculator_text'],
            $merchantProduct['calculator_link']));
        return $query;
    }

    /**
     * Method gets all merchant products from Database.
     * @return array Merchant products stored in database.
     */
    public function getAllMerchantProducts(): array
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product");
        return $query->rows;
    }

    /**
     * Method gets merchant product from Database based on ID.
     * @param $id int Merchant product ID.
     */
    public function getMerchantProductById(int $id): array | bool
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE id_merchant_product = '$id'");
        return !$query ? $query : $query->row;
    }

    /**
     * Method process update of merchant product in Database.
     * @param $merchantProduct array Merchant product data to be updated in Database.
     * @return mixed Indicator of successful operation.
     */
    public function updateMerchantProduct(array $merchantProduct): mixed
    {
        $query = $this->db->query(sprintf("UPDATE %sholmbank_merchant_product 
            SET type = '%s', 
                name = '%s', 
                description = '%s', 
                logo = '%s', 
                is_enabled = '%s', 
                is_calculator_displayed = '%s',
                interest_rate = '%s', 
                payment_period = '%s', 
                calculator_text = '%s',
                calculator_link = '%s'
            WHERE id_merchant_product = %s", DB_PREFIX, $merchantProduct['type'], $merchantProduct['name'],
            $merchantProduct['description'], $merchantProduct['logo'], (bool)$merchantProduct['is_enabled'], (bool)$merchantProduct['is_calculator_displayed'],
            (float)$merchantProduct['interest_rate'], (float)$merchantProduct['payment_period'], $merchantProduct['calculator_text'],
            $merchantProduct['calculator_link'], $merchantProduct['id_merchant_product']));

        return $query;
    }

    /**
     * Method process delete of merchant product from Database.
     * @param $id int Merchant product ID in Database.
     * @return bool Indicator of successful operation.
     */
    public function deleteMerchantProduct(int $id): bool
    {
        $query = $this->db->query("DELETE FROM " . DB_PREFIX . "holmbank_merchant_product WHERE id_merchant_product = '$id'");
        return $query;
    }
}