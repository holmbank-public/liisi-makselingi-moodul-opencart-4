<?php

namespace Opencart\Catalog\Controller\Extension\Holmbank\Payment;


/**
 * Holmbank Checkout Extension Controller.
 */
class Holmbank extends \Opencart\System\Engine\Controller {

    private $error = [];

    /**
     * Holmbank extension every payment method code left part.
     */
    private const PAYMENT_EXTENSION_CODE = 'holmbank';

    /**
     * Holmbank extension awaiting status name.
     */
    private const STATUS_AWAITING = 'HOLMBANK PAYMENT AWAITING';

    /**
     * Method responsible for processing GET requests. Basically displays different merchant products.
     * @return string
     */
    public function index(): string
    {
        $data['href_confirm'] = $this->url->link('extension/holmbank/payment/holmbank|confirm');

        return $this->load->view('extension/holmbank/payment/holmbank', $data);
    }

    /**
     * Add Header on Product Page.
     * @return void
     */
    public function addProductPageHeader(&$route, &$data) {
        // Add CSS to the template.
        $this->document->addStyle('extension/holmbank/catalog/view/stylesheet/payment.css');
    }

    /**
     * Method modify product page and adds calculator to it.
     * @return void
     */
    public function modifyProductPage(&$route, &$data)
    {
        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        // Check if the extension is enabled
        if (!$this->config->get('payment_holmbank_status')) {
            return;
        }

        $this->load->model('catalog/product');
        $this->load->language('extension/holmbank/payment/holmbank');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info && isset($product_info['price'])) {
            // Get only active merchant loan products to display to customer.
            $this->load->model('extension/holmbank/payment/holmbank');
            $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
            $merchantLoans = $holmbankRepository->getCalculatorMerchantProducts();

            $product_html = '';
            foreach ($merchantLoans as $merchantDBLoan) {
                // Check if interest rate and payment period are set at all.
                if ($merchantDBLoan['interest_rate'] == '' || $merchantDBLoan['payment_period'] == '' ||
                    $merchantDBLoan['interest_rate'] == 0 || $merchantDBLoan['payment_period'] == 0) {
                    continue;
                }
                $monthlyPayment = number_format((($product_info['price']) *
                        (1 + $merchantDBLoan['interest_rate'] / 100 * $merchantDBLoan['payment_period'] / 12)) / ($merchantDBLoan['payment_period']), 2);

                $merchant_product_data = [
                    'holmbank_payment_logo' => $merchantDBLoan['logo'],
                    'holmbank_payment_logo_alt' => $this->language->get('payment_method_logo'),
                    'holmbank_calculator_estimation' => $monthlyPayment,
                    'shop_currency_code' => $this->session->data['currency'],
                    'holmbank_calculator_description' => $merchantDBLoan['calculator_text'],
                    'holmbank_calculator_link' => $merchantDBLoan['calculator_link'],
                ];

                $product_html .= $this->load->view('extension/holmbank/payment/holmbank_product_calculator', $merchant_product_data);
            }

            $data['tax'] = isset($data['tax']) ? $data['tax'] . $product_html : $product_html;
        }
    }

    /**
     * Method responsible for Holmbank payment initialization.
     * @return void
     */
    public function confirm(): void
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {

            $this->load->language('extension/holmbank/payment/holmbank');

            $json = [];

            if (!isset($this->session->data['order_id']))
            {
                $json['error'] = $this->language->get('error_order');
            }

            $payment = explode('.', $this->session->data['payment_method']['code']);

            if (!isset($this->session->data['payment_method']) || $payment[0] != self::PAYMENT_EXTENSION_CODE)
            {
                $json['error'] = $this->language->get('error_payment_method');
            }

            // Chosen payment is correct, so we can now create order and send request to Holmbank.
            if (!$json)
            {
                $requestBody = $this->assembleRequestBody($this->cart, $this->session->data['order_id']);

                // Cart total should always be equal to sum of products.
                $isValid = $this->orderTotalValidation($requestBody);

                // Totals are not equal to each other, so display error message to the customer and send him back to the cart page.
                if (!$isValid)
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There are problems with your cart!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Control that loan product still exist in the backend.
                $isValid = $this->loanPaymentExistValidation($this->session->data['payment_method']['type']);

                // Product was deleted from API system, but still remains in Merchant System. Send customer back to cart.
                if (!$isValid)
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There is a problem with chosen payment method!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Order is valid and ready to process API requests.
                $uniqueKey = $this->generateUniqueKey($this->cart);

                // Initialize API Controller Calls - Start Payment Process
                require_once DIR_EXTENSION . 'holmbank/system/library/holmbank.php';
                $this->load->model('setting/setting');
                $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

                $holmbank_api = new \Opencart\System\Library\Holmbank($mySettingValue);
                $response = $holmbank_api->postLoanStart($uniqueKey, $requestBody);

                $response = json_decode($response, true);

                // API returned data is not matched with expectations, so send customer back to the cart.
                if (empty($response["orderLink"]) || empty($response["orderId"]))
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There is a problem with holmbank!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Load the necessary models
                $this->load->model('localisation/order_status');
                $this->load->model('checkout/order');

                $paymentData =
                [
                    "id_shop" => $this->config->get('config_store_id'),
                    "id_merchant_order" => $this->session->data['order_id'],
                    "id_holmbank_order" => $response["orderId"],
                    "x_payment_link_req_id" => $uniqueKey,
                ];

                $this->load->model('extension/holmbank/payment/holmbank');
                $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
                $holmbankRepository->addHolmbankOrder($paymentData);

                // Everything is fine. Create Order and Redirect User to start payment and clear cart.
                $this->load->model('checkout/order');
                $this->model_checkout_order->addHistory($this->session->data['order_id'], $this->config->get('payment_free_checkout_order_status_id'));

                // Everything is fine. Redirect User to start payment and clear cart.
                $order_statuses = $this->model_localisation_order_status->getOrderStatuses();
                foreach ($order_statuses as $order_status)
                {
                    if (strtoupper($order_status['name']) == self::STATUS_AWAITING)
                    {
                        $this->model_checkout_order->addHistory($this->session->data['order_id'], $order_status['order_status_id']);
                        break;
                    }
                }

                $this->cart->clear();
                $this->response->redirect($response["orderLink"]);
            }
        }
    }

    /**
     * Method responsible for Holmbank payment return.
     * @return void
     */
    public function return(): void
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            // Get POST body
            $key = $this->request->server['HTTP_X_PAYMENT_LINK_REQ_ID'];

            $raw = file_get_contents('php://input');
            $data = json_decode($raw);

            // POST comes without any data
            if (empty($data))
            {
                die();
            }

            // Get record from Database that Order exist in Database.
            $this->load->model('extension/holmbank/payment/holmbank');
            $holmbankRepository = $this->model_extension_holmbank_payment_holmbank;
            $holmbankOrder = $holmbankRepository->getHolmbankOrderByIdAndKey($data->orderId, $key);

            // Order was found in Database.
            if ($holmbankOrder->num_rows != 0)
            {
                // We need to change Order status based on Bank request data.
                $status = $data->status;
                $this->changeOrderStatus($status, $holmbankOrder->row['id_merchant_order']);
            }
            die();
        }

        $request = $this->request->get;
        if (isset($request['status']) && $request['orderId']) {
            if ($request['status'] == 'REJECTED') {
                $this->response->redirect($this->url->link('checkout/checkout', '', true));
            } else {
                $this->response->redirect($this->url->link('checkout/success', '', true));
            }
        }
    }

    /**
     * Method assemble request body to send it to the API.
     * - All cart products are added to request with final price after applying taxes.
     * - Cart rules are being added as separate products with negative values.
     * - Shipping is also being added as separate product.
     * @param $cart object Defines customer cart.
     * @return array Defines request body.
     */
    private function assembleRequestBody($cart, $idOrder): array
    {
        // NB! orderNumber can not be sent, because it's not created yet.

        // Order Totals
        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;

        $this->load->model('checkout/cart');
        ($this->model_checkout_cart->getTotals)($totals, $taxes, $total);

        $requestBody =
        [
            "language" => $this->language->get('code'),
            "totalAmount" => round($total, 2),
            "paymentType" => $this->session->data['payment_method']['type'],
            "products" => array(),
            "returnUrl" => $this->url->link('extension/holmbank/payment/holmbank|return')
        ];

        // Assemble body with all chosen by client products.
        $products = $cart->getProducts();
        $productsTotal = 0;

        foreach ($products as $product)
        {
            $productInfo =
            [
                "productSKU" => $product['product_id'],
                "productName" => $product['name'],
                "totalPrice" => $product['total'],
                "quantity" => $product['quantity'],
            ];
            $requestBody["products"][] = $productInfo;
            $productsTotal += $product['total'];
        }

        // Assemble body with all cart applied vouchers.
        if (!empty($this->session->data['voucher']))
        {
            $this->load->model('checkout/voucher');
            $voucherInfo = $this->model_checkout_voucher->getVoucher($this->session->data['voucher']);
            $productInfo =
                [
                    "productName" => 'voucher_' . $voucherInfo['code'],
                    "totalPrice" => -$voucherInfo['amount'],
                    "quantity" => 1,
                ];
            $requestBody["products"][] = $productInfo;
        }

        // Assemble body with all cart applied coupons.
        $isShippingFree = false;
        if (!empty($this->session->data['coupon']))
        {
            $this->load->model('marketing/coupon');
            $couponInfo = $this->model_marketing_coupon->getCoupon($this->session->data['coupon']);

            // Handle coupon shipping
            $isShippingFree = (bool) $couponInfo['shipping'];

            // Consider that coupon can be with fixed value or percentage 'F' and 'P'
            $couponType = $couponInfo['type'];
            $couponDiscount = $couponInfo['discount'];
            if (0 != $couponDiscount)
            {
                $totalPrice = ('P' === $couponType) ? $productsTotal * $couponDiscount / 100 : $couponDiscount;
                $productInfo =
                    [
                        "productName" => 'coupon_' . $couponInfo['code'],
                        "totalPrice" => -$totalPrice,
                        "quantity" => 1,
                    ];
                $requestBody["products"][] = $productInfo;
            }
        }

        // Get shipping Method Cost. NB! coupon can make shipping free.
        if ($this->cart->hasShipping() && !$isShippingFree)
        {
            if (isset($this->session->data['shipping_method']))
            {
                $requestBody["products"][] =
                    [
                        "productName" => $this->session->data['shipping_method']["code"],
                        "totalPrice" => $this->session->data['shipping_method']["cost"],
                        "quantity" => 1,
                    ];
            }
        }

        // Assemble taxes as product:
        foreach ($taxes as $tax)
        {
            $productInfo =
                [
                    "productSKU" => 'tax',
                    "productName" => 'tax',
                    "totalPrice" => round($tax, 2),
                    "quantity" => 1,
                ];
            $requestBody["products"][] = $productInfo;
        }

        // Eliminate small rounding differences
        $productsTotal = 0;
        foreach ($requestBody["products"] as $product)
        {
            $productsTotal += $product['totalPrice'];
        }

        $totalDifference = round($total - $productsTotal, 2);
        if (0 != $totalDifference)
        {
            $requestBody["products"][] =
                [
                    "productName" => "total_round_difference",
                    "totalPrice" => $totalDifference,
                    "quantity" => 1,
                ];
        }

        return $requestBody;
    }

    /**
     * Method checks if order matches Holmbank API criteria.
     * - "Cart total amount should always be equal to the products total sum in request"
     * @param $requestBody array Defines request to be sent to API.
     * @return bool Defines order suitability to be sent to the API.
     */
    private function orderTotalValidation($requestBody): bool
    {
        $productCostSum = 0;
        foreach ($requestBody['products'] as $product)
        {
            $productCostSum += $product['totalPrice'];
        }

        return $requestBody['totalAmount'] === round($productCostSum, 2);
    }

    /**
     * Method checks that chosen loan product still exist in API system.
     * @param $loanProductType
     * @return bool
     */
    private function loanPaymentExistValidation($loanProductType): bool
    {
        // API Controller
        require_once DIR_EXTENSION . 'holmbank/system/library/holmbank.php';
        $this->load->model('setting/setting');
        $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

        $holmbank_api = new \Opencart\System\Library\Holmbank($mySettingValue);
        $merchant_product_types = $holmbank_api->getLoanProducts();

        foreach ($merchant_product_types as $loanProduct)
        {
            if ($loanProduct['type'] === $loanProductType)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Method generate unique key for payment transaction.
     * @return string unique value generated by e-store
     */
    private function generateUniqueKey($cart): string
    {
        $cart_id = !empty($cart->cart_id) ? $cart->cart_id : 0;

        // Just to be sure, that values never be the same add cart id to the unique key.
        return bin2hex(random_bytes(40)) . $cart_id;
    }

    /**
     * Method handles order status changes.
     * @param $status string Defines status that come from API.
     * @param $order_id int Defines order ID which status should be changed.
     * @return void Process status changes.
     */
    private function changeOrderStatus($status, $order_id): void
    {
        switch ($status)
        {
            case "APPROVED":
                $newStateName = 'PROCESSING';
                break;
            case "PENDING":
                $newStateName = self::STATUS_AWAITING;
                break;
            case "REJECTED":
                $newStateName = 'DENIED';
                break;
            default:
                return;
        }

        // Update the order status
        $this->load->model('checkout/order');
        $this->load->model('localisation/order_status');

        $order_statuses = $this->model_localisation_order_status->getOrderStatuses();
        foreach ($order_statuses as $order_status) {
            if (strtoupper($order_status['name']) == $newStateName) {
                $this->model_checkout_order->addHistory($order_id, intval($order_status['order_status_id']));
                break;
            }
        }
    }
}