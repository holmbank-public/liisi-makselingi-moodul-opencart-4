<?php

namespace Opencart\Catalog\Model\Extension\Holmbank\Payment;


/**
 * Repository to process Database operations with Merchant Product Table.
 */
class Holmbank extends \Opencart\System\Engine\Model {

    /**
     * Function is responsible for displaying payment methods.
     * @param array $address
     * @return array
     */
    public function getMethods(array $address = []): array {

        $activePaymentMethods = $this->getActiveMerchantProducts();

        if ($this->cart->hasSubscription())
        {
            $status = false;
        } elseif (!$this->cart->hasShipping())
        {
            $status = false;
        } else
        {
            $status = true;
        }

        $method_data = [];

        if ($status) {
            $option_data = [];
            foreach($activePaymentMethods as $activePaymentMethod){
                $data =
                [
                    'merchant_product' => $activePaymentMethod,
                ];

                $option_data['holmbank_'.$activePaymentMethod['name']] =
                [
                    'code' => 'holmbank.'.'holmbank_'.$activePaymentMethod['name'],
                    'name' => $activePaymentMethod['name'],
                    'type' => $activePaymentMethod['type']
                ];
            }
            $method_data =
            [
                'code'       => 'holmbank.holmbank',
                'name'       => 'Holmbank',
                'option'     => $option_data,
                'sort_order' => 1
            ];
        }

        return $method_data;
    }

    /**
     * Method gets all merchant products from Database that are currently active.
     * @return array Merchant products stored in database.
     */
    public function getActiveMerchantProducts()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE is_enabled = true";
        return $this->db->query($query)->rows;
    }

    /**
     * Method gets all active for calculator merchant products from Database that are currently active.
     * @return array Merchant products stored in database.
     */
    public function getCalculatorMerchantProducts()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE is_enabled = true AND is_calculator_displayed = true";
        return $this->db->query($query)->rows;
    }

    /**
     * Method process insertion of holmbank order in Database.
     * @param $orderData array Holmbank order data to be inserted in Database.
     * @return mixed boolean as indicator of successful operation.
     */
    public function addHolmbankOrder($orderData)
    {
        $query = $this->db->query(sprintf("INSERT INTO %sholmbank_orders 
            (id_shop, id_merchant_order, id_holmbank_order, x_payment_link_req_id) VALUES 
            ('%s','%s','%s','%s')", DB_PREFIX, $orderData['id_shop'], (int) $orderData['id_merchant_order'],
            $orderData['id_holmbank_order'], $orderData['x_payment_link_req_id']));
        return $query;
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID.
     * @param $orderId string Holmbank order ID.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderById($orderId)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s"';
        return $this->db->query(sprintf($query, DB_PREFIX, $orderId));
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID and secret key.
     * @param $orderId string Holmbank order ID.
     * @param $uniqueKey string Transaction related secret key.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderByIdAndKey($orderId, $uniqueKey)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s" AND `x_payment_link_req_id`="%3$s"';
        return $this->db->query(sprintf($query, DB_PREFIX, $orderId, $uniqueKey));
    }
}